<?php


namespace App\Services;


use Symfony\Component\HttpFoundation\Request;

interface BlogPostServiceInterface
{
    /**
     * @param array   $params
     * @param Request $request
     *
     * @return bool
     */
    public function addBlogPost(array $params, Request $request): bool;


    /**
     * @param array $params
     *
     * @return bool
     */
    public function editBlogPost(array $params): bool;


    /**
     * @return array
     */
    public function listPostsByUserid(): array;


    /**
     * @param int $pageNumber
     *
     * @return array
     */
    public function listPosts(int $pageNumber): array;


    /**
     * @param int $id
     *
     * @return array
     */
    public function getBlogPostById(int $id): array;
}