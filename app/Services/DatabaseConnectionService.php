<?php

namespace  App\Services;

use App\Utilities\Environment\EnvironmentSetting;
use Exception;
use PDO;
use PDOException;

/**
 * Class DatabaseConnectionService
 *
 * @package App\Services
 */
class DatabaseConnectionService implements DatabaseConnectionServiceInterface
{
    private mixed $connection;


    /**
     * DatabaseConnectionService constructor.
     *
     * @param EnvironmentSetting $setting
     */
    public function __construct(
        private EnvironmentSetting $setting)
    {
        $this->connection = null;
    }


    /**
     * DB connection
     *
     * @return mixed|PDO
     * @throws Exception
     */
    public function getConnection()
    {
        try {
            if(is_null($this->connection)) {
                $dBSetting = $this->setting->getDatabaseSettings();
                $this->connection = new PDO($dBSetting->CONNECTION_STRING, $dBSetting->USER_NAME, $dBSetting->PASSWORD, [PDO::ATTR_PERSISTENT => true]);
            }
        } catch (PDOException $e) {
            throw new Exception("Connect failed: " .  $e->getMessage());
        }

        return $this->connection;
    }

}