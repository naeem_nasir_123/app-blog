<?php


namespace App\Models;


interface BlogPostModelInterface
{
    /**
     * @param array $params
     *
     * @return bool
     */
    public function addBlogPost(array $params): bool;


    /**
     * @param array $params
     *
     * @return bool
     */
    public function updateBlogPost(array $params): bool;


    /**
     * @return array
     */
    public function blogPostListByUserid(): array;


    /**
     * @param int $id
     *
     * @return array
     */
    public function getBlogPostById(int $id): array;


    /**
     * @param int $page
     * @param int $items_per_page
     *
     * @return array
     */
    public function blogPostList(int $page, int $items_per_page): array;
}