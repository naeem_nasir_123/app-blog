<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class BlogPostController extends BaseController
{
    const PAGE_NUMBER = 'page-number';


    /**
     * Render blog post overview
     *
     * @param         $id
     * @param string  $title
     * @param Request $request
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function postOverviewAction($id, string $title, Request $request): Response
    {
        return $this->renderView(
            getTwig()->render('blogPostOverview.html.twig', ['post' => $this->getBlogPostService()->getBlogPostById($id)])
        );
    }


    /**
     * Render all posts
     *
     * @param Request $request
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function postListeningAction(Request $request): Response
    {
        $pageNumber = $request->get(self::PAGE_NUMBER, 1);
        return $this->renderView(
            getTwig()->render('listBlogPost.html.twig', ['blogPosts' => $this->getBlogPostService()->listPosts($pageNumber)])
        );
    }
}