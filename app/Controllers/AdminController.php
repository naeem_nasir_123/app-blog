<?php

namespace App\Controllers;

use App\Services\BlogPostService;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AdminController extends BaseController
{
    /**
     * Render manage posts
     *
     * @param Request $request
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function managePostsAction(Request $request): Response
    {
        if (!isUserAuthenticated()) {
            redirect('/login');
        }

        return $this->renderView(
            getTwig()->render('manageBlogPost.html.twig', ['blogPosts' => $this->getBlogPostService()->listPostsByUserid()])
        );
    }


    /**
     * Render add post page
     *
     * @param Request $request
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function addPostsAction(Request $request): Response
    {
        if (!isUserAuthenticated()) {
            redirect('/login');
        }

        return $this->renderView(getTwig()->render('addBlogPost.html.twig', []));
    }


    /**
     * Api to add post
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addPostsApiAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        if (!isUserAuthenticated()) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            $response->setData('Not allowed');

            return $response;
        }

        $params = [
            BlogPostService::POST_TITLE => $request->get(BlogPostService::POST_TITLE, ''),
            BlogPostService::POST_TEXT  => $request->get(BlogPostService::POST_TEXT, ''),
        ];

        try {
            $result = $this->getBlogPostService()->addBlogPost($params, $request);
            if ($result === true) {
                $response->setStatusCode(Response::HTTP_CREATED);
                $response->setData('Post added');
            }
            else {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $response->setData('Unable to add post');
            }
        }
        catch (Exception $ex) {
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            $response->setData($ex->getMessage());
        }

        return $response;
    }


    /**
     * Render edit post page
     *
     * @param Request $request
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function editPostsAction(Request $request): Response
    {
        if (!isUserAuthenticated()) {
            redirect('/login');
        }

        $result = $this->getBlogPostService()->getBlogPostById($request->get('id'));

        return $this->renderView(getTwig()->render('editBlogPost.html.twig', ['post' => $result]));
    }


    /**
     * Edit post api
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function editPostsApiAction(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        if (!isUserAuthenticated()) {
            $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            $response->setData('Not allowed');

            return $response;
        }

        $params = [
            BlogPostService::POST_TITLE     => $request->get(BlogPostService::POST_TITLE, ''),
            BlogPostService::POST_TEXT      => $request->get(BlogPostService::POST_TEXT, ''),
            BlogPostService::POST_ID        => $request->get(BlogPostService::POST_ID, 0),
            BlogPostService::POST_IMAGE_URL => $request->get(BlogPostService::POST_IMAGE_URL),
        ];

        try {
            $result = $this->getBlogPostService()->editBlogPost($params);
            if ($result === true) {
                $response->setStatusCode(Response::HTTP_CREATED);
                $response->setData('Post updated');
            }
            else {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
                $response->setData('Unable to update post');
            }
        }
        catch (Exception $ex) {
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            $response->setData($ex->getMessage());
        }

        return $response;
    }
}