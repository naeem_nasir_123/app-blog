<?php


namespace App\Controllers;


use App\Services\AuthService;
use App\Services\BlogPostService;
use Exception;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BaseController
 *
 * @package App\Controllers
 */
class BaseController
{
    /**
     * @return AuthService
     * @throws Exception
     */
    protected function getAuthService(): AuthService
    {
        /** @var AuthService $authService */
        $authService = getContainer()->get('auth.service');
        return $authService;
    }


    /**
     * @return BlogPostService
     * @throws Exception
     */
    protected function getBlogPostService(): BlogPostService
    {
        /** @var BlogPostService $blogPostService */
        $blogPostService = getContainer()->get('blog.post.service');

        return $blogPostService;
    }


    /**
     * @param string $view
     *
     * @return Response
     */
    protected function renderView(string $view): Response
    {
        return new Response($view);
    }

}