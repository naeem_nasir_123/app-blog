<?php

namespace App\Controllers;

use App\Services\AuthService;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class AuthController extends BaseController
{
    /**
     * Render login page
     *
     * @param Request $request
     *
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function loginAction(Request $request): Response
    {
        return $this->renderView(getTwig()->render('login.html.twig', []));
    }

    /**
     * Logout
     *
     * @param Request $request
     */
    public function logoutAction(Request $request): void
    {
        if ($this->getAuthService()->logout()){
            redirect('/');
        }
    }


    /**
     * User login api
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function userLoginAction(Request $request): JsonResponse
    {
        $response    = new JsonResponse();

        $params = [
            AuthService::AUTH_USERNAME => $request->get(AuthService::AUTH_USERNAME, ''),
            AuthService::AUTH_PASSWORD => $request->get(AuthService::AUTH_PASSWORD, ''),
        ];

        try {
            $result = $this->getAuthService()->getUserByEmailAndPassword($params);
            if ($result === true) {
                $response->setStatusCode(Response::HTTP_OK);
                $response->setData('User logedIn');
            }
            else {
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                $response->setData('User not recognised');
            }
        }
        catch (Exception $ex) {
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            $response->setData($ex->getMessage());
        }

        return $response;
    }
}