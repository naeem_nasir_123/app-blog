<?php

namespace Test\Services;

use App\Models\BlogPostModelInterface;
use App\Services\BlogPostService;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use function uniqid;

class BlogPostServiceTest extends TestCase
{
    /**
     * @var BlogPostModelInterface | MockObject
     */
    private BlogPostModelInterface $blogPostModel;

    private BlogPostService $blogPostService;


    public function setUp(): void
    {
        $this->blogPostModel   = $this->createMock(BlogPostModelInterface::class);
        $this->blogPostService = new BlogPostService($this->blogPostModel, 10, '/public/');
    }


    /**
     * @throws Exception
     */
    public function testEditBlogPostWithMissinParameters(): void
    {
        $params = [
            'title' => uniqid('title', true)
        ];

        $this->blogPostModel->expects(static::never())
            ->method('updateBlogPost');

        static::expectException(Exception::class);
        static::expectExceptionMessage('Missing parameters');

        $this->blogPostService->editBlogPost($params);

    }


    /**
     * @throws Exception
     */
    public function testEditBlogPostWithEmptyParameterValue(): void
    {
        $params = [
            'title' => '',
            'text'  => uniqid('title', true),
            'id'    => random_int(10, 100 * 20),
        ];

        $this->blogPostModel->expects(static::never())
            ->method('updateBlogPost');

        static::expectException(Exception::class);
        static::expectExceptionMessage('Empty parameters value!');

        $this->blogPostService->editBlogPost($params);

    }


    /**
     * @throws Exception
     */
    public function testEditBlogPostFailed(): void
    {
        $params = [
            'title' => uniqid('title', true),
            'text'  => uniqid('title', true),
            'id'    => random_int(10, 100 * 20),
        ];

        $this->blogPostModel->expects(static::once())
            ->method('updateBlogPost')
            ->with(static::equalTo($params))
            ->willReturn(false);

        static::assertFalse($this->blogPostService->editBlogPost($params));
    }


    /**
     * @throws Exception
     */
    public function testEditBlogPostSuccess(): void
    {
        $params = [
            'title' => uniqid('title', true),
            'text'  => uniqid('title', true),
            'id'    => random_int(10, 100 * 20),
        ];

        $this->blogPostModel->expects(static::once())
            ->method('updateBlogPost')
            ->with(static::equalTo($params))
            ->willReturn(true);

        static::assertTrue($this->blogPostService->editBlogPost($params));
    }


    /**
     * @throws Exception
     */
    public function testListPostsByUserid(): void
    {
        $result = [
            'date' => uniqid('test', true),
        ];
        $this->blogPostModel->expects(static::once())
            ->method('blogPostListByUserid')
            ->willReturn($result);

        static::assertSame($result, $this->blogPostService->listPostsByUserid());
    }


    /**
     * @throws Exception
     */
    public function testListPostsByUseridEmptyResult(): void
    {
        $result = [];
        $this->blogPostModel->expects(static::once())
            ->method('blogPostListByUserid')
            ->willReturn($result);

        static::assertEmpty($this->blogPostService->listPostsByUserid());
    }


    /**
     * @throws Exception
     */
    public function testListPosts(): void
    {
        $pageNumber = 10;
        $result     = [
            'date' => uniqid('test', true),
        ];
        $this->blogPostModel->expects(static::once())
            ->method('blogPostList')
            ->with(static::equalTo($pageNumber), static::equalTo(10))
            ->willReturn($result);

        static::assertSame($result, $this->blogPostService->listPosts($pageNumber));
    }


    /**
     * @throws Exception
     */
    public function testListPostsEmptyResult(): void
    {
        $pageNumber = 10;
        $result     = [];
        $this->blogPostModel->expects(static::once())
            ->method('blogPostList')
            ->with(static::equalTo($pageNumber), static::equalTo(10))
            ->willReturn($result);

        static::assertSame($result, $this->blogPostService->listPosts($pageNumber));
    }


    /**
     * @throws Exception
     */
    public function testGetBlogPostById(): void
    {
        $id     = random_int(10, 100 * 100);
        $result = [
            'date' => uniqid('test', true),
        ];
        $this->blogPostModel->expects(static::once())
            ->method('getBlogPostById')
            ->with(static::equalTo($id))
            ->willReturn($result);

        static::assertSame($result, $this->blogPostService->getBlogPostById($id));
    }


    /**
     * @throws Exception
     */
    public function testGetBlogPostEmptyResult(): void
    {
        $id     = random_int(10, 100 * 100);
        $result = [];
        $this->blogPostModel->expects(static::once())
            ->method('getBlogPostById')
            ->with(static::equalTo($id))
            ->willReturn($result);

        static::assertSame($result, $this->blogPostService->getBlogPostById($id));
    }

}