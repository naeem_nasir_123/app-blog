<?php


use Phinx\Seed\AbstractSeed;

class UsersSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'username' => 'main_user',
                'first_name' => 'Naeem',
                'last_name' => 'Nasir',
                'password' => getHashedValue('blog123'),
                'created' => date('Y-m-d H:i:s'),
                'email' => 'naeem.nasir877@gmail.com',
            ],
            [
                'username' => 'test.user',
                'first_name' => 'Test',
                'last_name' => 'User',
                'password' => getHashedValue('blog_app_123'),
                'created' => date('Y-m-d H:i:s'),
                'email' => 'test.user@gmail.com',
            ],
            [
                'username' => 'xentral',
                'first_name' => 'xentral',
                'last_name' => 'ERP',
                'password' => getHashedValue('blog_123'),
                'created' => date('Y-m-d H:i:s'),
                'email' => 'xentral@gmail.com',
            ]
        ];

        $users = $this->table('users');
        $users->insert($data)
            ->saveData();
    }
}
