<?php
/***
 * Environment Configuration of the application
 *
 */
return [
    'DEBUG_MODE' => true,
    'DB' => [
        'NAME' => 'blog_app',
        'USER_NAME' => 'root',
        'PASSWORD' => 'root',
        'HOST' => 'localhost'
    ]
];
