const selectors = {
    username: '#username',
    password: '#password',
    title: '#title',
    text: '#text',
    imageUrl: '#image_url',
};

function userLogin() {
    var form_data = new FormData();
    form_data.append("username", $(selectors.username).val());
    form_data.append("password", $(selectors.password).val());

    $.ajax({
        url: "/user-login",
        type: "POST",
        processData: false,
        contentType: false,
        data: form_data,
        success:
            function (result) {
                window.location.replace("/admin");
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Failure',
                    text: result.responseJSON,
                    type: 'error'
                });
            }
    });
}

function addPost() {
    var file_data = $(selectors.imageUrl);
    var form_data = new FormData();
    form_data.append('image_url', file_data[0].files[0]);
    form_data.append("title", $(selectors.title).val());
    form_data.append("text", $(selectors.text).val());
        $.ajax({
        url: "/add-blog-post-api",
        type: "POST",
        processData: false,
        contentType: false,
        data: form_data,
        success:
            function (result) {
                new PNotify({
                    title: 'Added',
                    text: result,
                    type: 'success'
                });
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Validation Failure',
                    text: result.responseJSON,
                    type: 'error'
                });
            }
    });
}

function editPost(id) {
    var form_data = new FormData();
    form_data.append('image_url', $(selectors.imageUrl).val());
    form_data.append("title", $(selectors.title).val());
    form_data.append("text", $(selectors.text).val());
    form_data.append("id", id);
    $.ajax({
        url: "/edit-blog-post-api",
        type: "POST",
        processData: false,
        contentType: false,
        data: form_data,
        success:
            function (result) {
                new PNotify({
                    title: 'Added',
                    text: result,
                    type: 'success'
                });
            },
        error:
            function (result) {
                new PNotify({
                    title: 'Validation Failure',
                    text: result.responseJSON,
                    type: 'error'
                });
            }
    });
}