# blog-app
Blog application for Xentral

# Installation

####  1. git clone https://naeem_nasir_123@bitbucket.org/naeem_nasir_123/app-blog.git

#### 2 Requirements

########## 2.1. PHP 8 is needed to run this project
########## 2.2. fileinfo, extension=gd, intl, mbstring, openssl and pdo_mysql extensions are needed to run this project


#### 2. Create a database and update env.php & phinx.xml file with database configuration:

```bash
'DB' => [
        'NAME' => 'blog_app',
        'USER_NAME' => 'root',
        'PASSWORD' => 'root',
        'HOST' => 'localhost'
    ]

 development:
        adapter: mysql
        host: localhost
        name: blog_app
        user: root
        pass: 'root'
        port: 3306
        charset: utf8
```

#### 3. Install composer packages:

```bash
$ composer install 
```
#### 5. Create Database schema:

```bash
$ php vendor/bin/phinx migrate
```

#### 6. Insert initial users data:

```bash
$ php vendor/bin/phinx seed:run
```
see login data at
\UsersSeeder::run


#### 7. Run PHP server:

```bash
$ php -S localhost:9000


#### 8. Run unittests:

```bash
$ ./vendor/bin/phpunit tests


#### 8. Pagination in backend:

I have implemented the pagination in the backend for blog listenings, please access the page like

http://localhost:9000/?page-number=2
http://localhost:9000/?page-number=3

